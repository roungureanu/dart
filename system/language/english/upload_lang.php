<?php

$lang['upload_userfile_not_set'] = "Unable to find a post variable called userfile.";
$lang['upload_file_exceeds_limit'] = "Imaginea depaseste limita de spatiu admisa.";
$lang['upload_file_exceeds_form_limit'] = "Imaginea depaseste limita de spatiu admisa.";
$lang['upload_file_partial'] = "The file was only partially uploaded.";
$lang['upload_no_temp_directory'] = "The temporary folder is missing.";
$lang['upload_unable_to_write_file'] = "The file could not be written to disk.";
$lang['upload_stopped_by_extension'] = "The file upload was stopped by extension.";
$lang['upload_no_file_selected'] = " ";
$lang['upload_invalid_filetype'] = "Tipul de fisiere pe care incerci sa il uploadezi nu este permis.";
$lang['upload_invalid_filesize'] = "Imaginea depaseste limita de spatiu admisa.";
$lang['upload_invalid_dimensions'] = "Imaginea depaseste dimensiunile admise.";
$lang['upload_destination_error'] = "A problem was encountered while attempting to move the uploaded file to the final destination.";
$lang['upload_no_filepath'] = "The upload path does not appear to be valid.";
$lang['upload_no_file_types'] = "You have not specified any allowed file types.";
$lang['upload_bad_filename'] = "The file name you submitted already exists on the server.";
$lang['upload_not_writable'] = "The upload destination folder does not appear to be writable.";


/* End of file upload_lang.php */
/* Location: ./system/language/english/upload_lang.php */