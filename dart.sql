-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 15, 2015 at 06:42 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dart`
--

-- --------------------------------------------------------

--
-- Table structure for table `anunturi`
--

CREATE TABLE IF NOT EXISTS `anunturi` (
`id` int(11) NOT NULL,
  `titlu` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `anunturi`
--

INSERT INTO `anunturi` (`id`, `titlu`, `text`, `data`) VALUES
(1, 'asd', 'asd', '2015-05-03'),
(2, 'anunt', 'adaugat', '2015-05-03');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
`id` int(11) NOT NULL,
  `postid` int(11) NOT NULL,
  `utilizator` varchar(255) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `postid`, `utilizator`, `text`) VALUES
(1, 2, 'rob', 'asd'),
(2, 2, 'rob', 'asd'),
(3, 9, 'rob', 'yay'),
(4, 17, 'Iulian', 'Ce culori frumoase!'),
(5, 17, 'Robert', 'Frumoasa imaginea!');

-- --------------------------------------------------------

--
-- Table structure for table `concurs`
--

CREATE TABLE IF NOT EXISTS `concurs` (
`id` int(11) NOT NULL,
  `data` date NOT NULL,
  `tema` varchar(255) NOT NULL,
  `detalii` text NOT NULL,
  `datasfarsit` date NOT NULL,
  `nume` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `concurs`
--

INSERT INTO `concurs` (`id`, `data`, `tema`, `detalii`, `datasfarsit`, `nume`) VALUES
(1, '0000-00-00', 'Test', 'asdasd', '2015-05-15', 'Test'),
(2, '0000-00-00', 'concurs', 'adaugat', '2015-05-01', 'concurs'),
(3, '0000-00-00', 'concurs', 'adaugat', '2015-05-28', 'concurs2');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
`id` int(11) NOT NULL,
  `concurs` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `titlu` varchar(255) NOT NULL,
  `sursa` varchar(255) NOT NULL,
  `data` date NOT NULL,
  `utilizator` varchar(255) NOT NULL,
  `likes` int(11) NOT NULL,
  `dislikes` int(11) NOT NULL,
  `likeutilizatori` text NOT NULL,
  `dislikeutilizatori` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `concurs`, `score`, `titlu`, `sursa`, `data`, `utilizator`, `likes`, `dislikes`, `likeutilizatori`, `dislikeutilizatori`) VALUES
(16, -1, 0, 'Avatar', '2.jpg', '2015-05-03', 'Admin', 0, 0, '', ''),
(17, 0, 2, 'Japan View', '3.jpg', '2015-05-03', 'Admin', 2, 0, '*Admin**Iulian*', ''),
(18, 0, 1, 'Rocks', '4.jpg', '2015-05-03', 'Admin', 1, 0, '*Iulian*', ''),
(19, 0, -1, 'Ice', '5.jpg', '2015-05-03', 'Admin', 0, 1, '', '*Admin*'),
(20, 0, 1, 'Airplanes', '6.jpg', '2015-05-03', 'Admin', 1, 0, '*Admin*', ''),
(21, 0, 0, 'Banana', '7.jpg', '2015-05-03', 'Admin', 0, 0, '', ''),
(22, 0, 0, 'Shaman King', '8.jpg', '2015-05-03', 'Admin', 0, 0, '', ''),
(23, 0, 1, 'Velocity * Time', '9.jpg', '2015-05-03', 'Robert', 1, 0, '*Robert*', ''),
(24, 0, 0, 'Penguins', '10.jpg', '2015-05-03', 'Robert', 0, 0, '', ''),
(25, 0, 0, 'Twolips', '11.jpg', '2015-05-03', 'Iulian', 0, 0, '', ''),
(26, 0, 0, 'Benjamn Franklyn', '12.jpg', '2015-05-03', 'Iulian', 0, 0, '', ''),
(27, 0, 0, 'Purple unicorn', '13.jpg', '2015-05-03', 'Iulian', 0, 0, '', ''),
(28, -1, 0, 'Avatar', '14.jpg', '2015-05-03', 'Iulian', 0, 0, '', ''),
(29, -1, 0, 'Avatar', '15.jpg', '2015-05-03', 'Robert', 0, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `utilizator` varchar(255) NOT NULL,
  `parola` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `nume` varchar(255) NOT NULL,
  `prenume` varchar(255) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `tip` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `utilizator`, `parola`, `email`, `nume`, `prenume`, `avatar`, `tip`) VALUES
(5, 'Robert', '202cb962ac59075b964b07152d234b70', 'a@a.com', 'Ungureanu', 'Robert', '', 1),
(6, 'Iulian', '202cb962ac59075b964b07152d234b70', 'b@b.com', 'Petrariu', 'Iulian', '', 1),
(7, 'Admin', '202cb962ac59075b964b07152d234b70', 'c@c.com', 'Admin', 'Admin', '', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anunturi`
--
ALTER TABLE `anunturi`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `concurs`
--
ALTER TABLE `concurs`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anunturi`
--
ALTER TABLE `anunturi`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `concurs`
--
ALTER TABLE `concurs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
