<?php

	Class Display extends CI_Controller{

		public function index(){
			$this->load->view('sumerror');
		}

		public function post($id){

			$this->load->model("get");
			$res = $this->get->verify_exist($id,'post');
			if(is_numeric($id) && !empty($res[0]) && $res[0]['titlu']!="Avatar" ){
			$pid = $id;
			$img = $this->get->post($pid);
			$comms = $this->get->comments($pid);
			$k = 0;
			$avt = array();
			foreach($comms as $comment){
				$avt[$k] = $this->get->avatar($comment['utilizator']);
				$k++;
			}

			$data['avatars'] = $avt;
			$data['post'] = $img[0];
			$data['comments'] = $comms;

			$this->load->view('post',$data);
			}
			else
				$this->load->view('sumerror');
		}

		

		public function globalimg($id = NULL){

		if($id == NULL || is_numeric($id)){
			$this->load->model("get");
			$this->load->library('pagination');


	$config['base_url'] = base_url("/display/globalimg");
	$config['total_rows'] =  $this->db->get_where('post',array('concurs' => 0))->num_rows();
	$config['per_page'] = 15; 
	$config['num_links'] = 2;
	$config['cur_tag_open'] = '<b style="color:white">';	
	$config['cur_tag_close'] = '</b>';
	$config['display_pages'] = TRUE;

	$this->pagination->initialize($config); 
	$this->db->order_by("score", "desc"); 
	$this->db->select('sursa,id,titlu');

	$data = $this->db->get_where('post',array('concurs' => 0), $config['per_page'],  $this->uri->segment(3));

	$k=0;
	$x=array();
	foreach($data->result_array() as $row)
		{$x['posts'][$k]['sursa'] = $row['sursa'];
		$x['posts'][$k]['id'] = $row['id'];
		$x['posts'][$k]['titlu'] = $row['titlu'];
		$k++;
		}
	$x['links'] = $this->pagination->create_links();

	$this->load->view('global',$x);
	}
	else
		$this->load->view('sumerror');

		}

		public function myimgs($id = NULL){

		if($id == NULL || is_numeric($id)){
			$this->load->model("get");
			$this->load->library('pagination');

	$config['base_url'] = base_url("/display/myimgs");
	$config['total_rows'] =  $this->db->get_where('post',array('concurs' => 0))->num_rows();
	$config['per_page'] = 15; 
	$config['num_links'] = 2;
	$config['cur_tag_open'] = '<b style="color:white">';	
	$config['cur_tag_close'] = '</b>';
	$config['display_pages'] = TRUE;

	$this->pagination->initialize($config); 
	$this->db->order_by("score", "desc"); 
	$this->db->select('sursa,id,titlu');

	$user = $this->session->userdata("logged_in");
	$data = $this->db->get_where('post',array('utilizator' => $user), $config['per_page'],  $this->uri->segment(3));

	$k=0;
	$x=array();
	foreach($data->result_array() as $row)
		{$x['posts'][$k]['sursa'] = $row['sursa'];
		$x['posts'][$k]['id'] = $row['id'];
		$x['posts'][$k]['titlu'] = $row['titlu'];
		$k++;
		}
	$x['links'] = $this->pagination->create_links();

	$this->load->view('myimgs',$x);
	}
	else
		$this->load->view('sumerror');

		}

		public function random($id = NULL){

		if($id == NULL || is_numeric($id)) {
			$this->load->model("get");
			$this->load->library('pagination');


	$config['base_url'] = 'http://localhost/dart/display/random';
	$config['total_rows'] =  $this->db->get_where('post',array('concurs' => 0))->num_rows();
	$config['per_page'] = 15; 
	$config['num_links'] = 2;
	$config['cur_tag_open'] = '<b style="color:white">';	
	$config['cur_tag_close'] = '</b>';
	$config['display_pages'] = TRUE;

	$this->pagination->initialize($config); 
		$this->db->order_by("id", "random"); 
	$this->db->select('sursa,id,titlu');

	$data = $this->db->get_where('post',array('concurs' => 0), $config['per_page'],  $this->uri->segment(3));

	$x=array();
	$k=0;
	foreach($data->result_array() as $row)
		{$x['posts'][$k]['sursa'] = $row['sursa'];
		$x['posts'][$k]['id'] = $row['id'];
		$x['posts'][$k]['titlu'] = $row['titlu'];
		$k++;
		}
	$x['links'] = $this->pagination->create_links();

	$this->load->view('random',$x);
	}
	else
		$this->load->view('sumerror');

		}

		public function newimg($id = NULL){

			$this->load->model("get");
			$this->load->library('pagination');
	if($id == NULL || is_numeric($id)){

	$config['base_url'] = 'http://localhost/dart/display/newimg';
	$config['total_rows'] = $this->db->get_where('post',array('concurs' => 0))->num_rows();
	$config['per_page'] = 15; 
	$config['num_links'] = 2;
	$config['cur_tag_open'] = '<b style="color:white">';	
	$config['cur_tag_close'] = '</b>';
	$config['display_pages'] = TRUE;

	$this->pagination->initialize($config); 
		$this->db->order_by("data", "desc"); 
	$this->db->select('sursa,id,titlu');

	$data = $this->db->get_where('post',array('concurs' => 0), $config['per_page'],  $this->uri->segment(3));

	$x=array();
	$k=0;
	foreach($data->result_array() as $row)
		{$x['posts'][$k]['sursa'] = $row['sursa'];
		$x['posts'][$k]['id'] = $row['id'];
		$x['posts'][$k]['titlu'] = $row['titlu'];
		$k++;
		}
	$x['links'] = $this->pagination->create_links();

	$this->load->view('newimage',$x);
	}
	else
		$this->load->view('sumerror');

		}		

		public function contest($id){

		$this->load->model('get');
		$res = $this->get->verify_exist($id,'concurs');
		if(is_numeric($id) && !empty($res[0]) ){
			$this->load->library('pagination');


	$config['base_url'] = base_url("/display/contest/");
	$config['total_rows'] = $this->db->get_where('post',array('concurs' => $id))->num_rows();
	$config['per_page'] = 15; 
	$config['num_links'] = 2;
	$config['cur_tag_open'] = '<b style="color:white">';	
	$config['cur_tag_close'] = '</b>';
	$config['display_pages'] = TRUE;

	$this->pagination->initialize($config); 
	$this->db->order_by("score", "desc"); 
	$this->db->select('sursa,id,titlu');

	$data = $this->db->get_where('post',array('concurs' => $id), $config['per_page'],  $this->uri->segment(4));

	$this->db->select('nume');
	$nume = $this->db->get_where('concurs',array('id' => $id));

	$titluconcurs = $nume->result_array();

	$k=0;
	$x=array();
	$x['posts'][$k]['contest'] = $titluconcurs[0];
	foreach($data->result_array() as $row)
		{$x['posts'][$k]['sursa'] = $row['sursa'];
		$x['posts'][$k]['id'] = $row['id'];
		$x['posts'][$k]['titlu'] = $row['titlu'];
		$k++;
		}
	$x['links'] = $this->pagination->create_links();

	$this->load->view('contestsimg',$x);
	}
	else
		$this->load->view('sumerror');

		}

	}
?>