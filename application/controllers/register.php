<?php

	Class Register extends CI_Controller{

		public function index(){
			$this->load->view('inregistrare');
		}

		public function inregistrare($id = NULL){

		if($id == NULL){
		$this->form_validation->set_rules("utilizator","Utilizator","required|callback_verify_utilizator");
		$this->form_validation->set_rules("parola","Parola","required");
		$this->form_validation->set_rules("cparola","Confirma Parola","required|callback_verify_potrivire_parola[".$this->input->post('parola')."]");
		$this->form_validation->set_rules("email","Email","required|valid_email");
		$this->form_validation->set_rules("cemail","Confirma Email","required|callback_verify_potrivire_email[".$this->input->post('email')."]");
		$this->form_validation->set_rules("nume","Nume","required");
		$this->form_validation->set_rules("prenume","Prenume","required");
		$this->form_validation->set_message("required", "Campul - %s - este obligatoriu.");
		$this->form_validation->set_message("valid_email", "Email-ul introdus este incorect.");
		
		if($this->form_validation->run() == FALSE){
			$this->load->view('inregistrare');
		} else {
			$utilizator = $this->input->post('utilizator');
			$parola = $this->input->post('parola');
			$email = $this->input->post('email');
			$nume = $this->input->post('nume');
			$prenume = $this->input->post('prenume');
			$tip = 1;
			$this->load->model("set");
			if ($this->set->insert_user($utilizator, $parola, $email, $nume, $prenume, $tip)) {
				$this->set->first_avatar($utilizator);
				redirect(base_url());
			} else
				echo "EROARE";

		}}
		else
			$this->load->view('sumerror');
	}

	public function verify_potrivire_parola($cparola, $parola){
		if($parola == $cparola)
			return true;
		else
		{
			$this->form_validation->set_message("verify_potrivire_parola","Cele doua parole nu se potrivesc");
			return false;
		}
	}

	public function verify_potrivire_email($email, $cemail){

		if($email == $cemail)
			return true;
		else
		{
			$this->form_validation->set_message("verify_potrivire_email","Email-urile nu se potrivesc");
			return false;
		}

	}

	public function verify_utilizator($utilizator){
		$utilizatori[]=array(0);
		 $query = $this->db->query('SELECT utilizator FROM users');
		 $k=0;
    foreach($query->result_array() as $row)
        $utilizatori[$k++]=$row['utilizator'];
    if (in_array($utilizator,$utilizatori)){
	$this->form_validation->set_message("verify_utilizator","%s exista deja in baza noastra de date");
	return false;
	} else {
		return true;
	}
}

	public function verify_email($email){
		$emailuri[]=array(0);
		 $query = $this->db->query('SELECT email FROM users');
		 $k=0;
    foreach($query->result_array() as $row)
        $emailuri[$k++]=$row['email'];
    if (in_array($email,$emailuri)){
	$this->form_validation->set_message("verify_email","%s exista deja in baza noastra de date");
	return false;
	} else {
		return true;
	}
}

	}

?>