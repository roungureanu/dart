<?php

	Class Login extends CI_Controller{

		public function index(){
			$this->load->view('signin');
		}

		public function signin($id = NULL){

		$user = $this->session->userdata("logged_in");
		if($id == NULL){
		if($user) {
			$this->load->view('alreadylogged');
		} else {

		$this->form_validation->set_rules("utilizator","Utilizator","required|callback_verify_utilizator_potrivire");
		$this->form_validation->set_rules("parola","Parola","required|callback_verify_parola_potrivire");
		$this->form_validation->set_message("required", "Campul - %s - este obligatoriu");

		if($this->form_validation->run() == FALSE){
			$this->load->view('signin');
		} else {
			$utilizator = $this->input->post('utilizator');

			$this->load->model("get");
			$q = $this->get->user($utilizator);
			$tip = $q[0]['tip'];

			$parola = $this->input->post('parola');
			$this->load->model('get');
			if($user = $this->get->login($utilizator,$parola)) {
					$this->session->set_userdata('logged_in',$utilizator);
					$this->session->set_userdata('tip',$tip);

					$this->load->view('main');
					redirect(base_url());
				} else {
					$this->load->view('signin');
					redirect(base_url());
				}

			}

		}
		}
		else
			$this->load->view('sumerror');
	}

	public function verify_utilizator_potrivire($utilizator){
		$utilizatori[]=array(0);
		 $query = $this->db->query('SELECT utilizator FROM users');
		 $k=0;
    foreach($query->result_array() as $row)
        $utilizatori[$k++]=$row['utilizator'];
    if (in_array($utilizator,$utilizatori)){
		return true;
	} else {
		$this->form_validation->set_message("verify_utilizator_potrivire","%s nu exista.");
	return false;
	}
}

	public function verify_parola_potrivire($parola){
		$parole[]=array(0);
		$utilizatori[]=array(0);
		$utilizator = $this->input->post('utilizator');
		 $query = $this->db->query('SELECT * FROM users');
		 $k=0;
    foreach($query->result_array() as $row){
        $parole[$k]=$row['parola'];
        $utilizatori[$k]=$row['utilizator'];
    
    if ($utilizator == $utilizatori[$k] && md5($parola) == $parole[$k])
		return true;
	else
		$k++;

	} 

	$this->form_validation->set_message("verify_parola_potrivire","Datele introduse sunt incorecte.");
	return false;
	
}

	public function logout(){
		$this->session->unset_userdata("logged_in");
		$this->session->unset_userdata("tip");
		redirect(base_url());
	}



	}

?>