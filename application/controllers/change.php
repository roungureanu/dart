<?php

	Class Change extends CI_Controller{

		public function index(){
			$this->load->view('sumerror');
		}

		public function password($user = NULL){
			$userlogged = $this->session->userdata("logged_in");
			if($userlogged == $user && $user!=NULL){
		$this->form_validation->set_rules("parola","Parola Curenta","required|callback_verify_parola");
		$this->form_validation->set_rules("nparola","Parola noua","required");
		$this->form_validation->set_rules("ncparola","Parola noua confirmata","required|callback_verify_potrivire_parola[".$this->input->post('nparola')."]");
		$this->form_validation->set_message("required", "Campul - %s - este obligatoriu.");

		if($this->form_validation->run() == FALSE){
			$this->load->model('get');
			$data['avatar'] = $this->get->avatar($user);
			$this->load->view('settings',$data);
		} else {
			$nparola = $this->input->post('nparola');
			$this->load->model("set");
			if ($this->set->update_password($nparola)) {
				redirect(base_url());
			} else
				echo "EROARE";

		}
	}
		else
			$this->load->view('sumerror');

		}

		public function verify_parola(){
		$user = $this->session->userdata("logged_in");
		$this->db->select('utilizator');
		$this->db->select('parola');
		$parola = $this->input->post('parola');
		$query = $this->db->get('users');
		$ok = 0;
		foreach($query->result_array() as $row){
			if($row['utilizator'] == $user)
				if($row['parola'] == md5($parola)){
					$ok = 1;
					break;}
		}

		if($ok==1)
			return true;
		else
		{
			$this->form_validation->set_message("verify_parola","Parola curenta introdusa este gresita.");
			return false;
		}
	}

		public function verify_potrivire_parola($parola, $cparola){
		if($parola == $cparola)
			return true;
		else
		{
			$this->form_validation->set_message("verify_potrivire_parola","Cele doua parole nu se potrivesc");
			return false;
		}
	}

	public function email($user = NULL){
		$userlogged = $this->session->userdata("logged_in");
		if($userlogged == $user && $user!=NULL){
		$this->form_validation->set_rules("parolae","Parola curenta","required|callback_verify_parolae");
		$this->form_validation->set_rules("nemail","Email-ul nou","required");
		$this->form_validation->set_rules("ncemail","Email-ul nou confirmat","required|callback_verify_potrivire_email[".$this->input->post('nemail')."]");
		$this->form_validation->set_message("required", "Campul - %s - este obligatoriu.");

		if($this->form_validation->run() == FALSE){
			$this->load->model('get');
			$data['avatar'] = $this->get->avatar($user);
			$this->load->view('settings',$data);
		} else {
			$nemail = $this->input->post('nemail');
			$this->load->model("set");
			if ($this->set->update_email($nemail)) {
				redirect(base_url());
			} else
				echo "EROARE";

		}}
		else
			$this->load->view('sumerror');

		}

		public function verify_potrivire_email($email, $cemail){
		if($email == $cemail)
			return true;
		else
		{
			$this->form_validation->set_message("verify_potrivire_email","Cele doua email-uri nu se potrivesc");
			return false;
		}
	}

	public function verify_parolae(){
		$user = $this->session->userdata("logged_in");
		$this->db->select('utilizator');
		$this->db->select('parola');
		$parola = $this->input->post('parolae');
		$query = $this->db->get('users');
		$ok = 0;
		foreach($query->result_array() as $row){
			if($row['utilizator'] == $user)
				if($row['parola'] == md5($parola)){
					$ok = 1;
					break;}
		}

		if($ok==1)
			return true;
		else
		{
			$this->form_validation->set_message("verify_parolae","Parola curenta introdusa este gresita.");
			return false;
		}
	}

	}
?>