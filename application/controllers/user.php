<?php

	Class User extends CI_Controller
	{

		public function index()
		{
			$this->load->view('sumerror');
		}


		public function upload($id = NULL)
		{
			
			$user = $this->session->userdata("logged_in");

			if($user && ($id == NULL || is_numeric($id))) 
			{	
				$this->form_validation->set_rules("titlu","Titlu","required");
				$this->form_validation->set_message("required", "Campul - %s - este obligatoriu.");

				$config['upload_path'] = './uploads/';
				$config['allowed_types'] = 'jpg';
				$config['max_size']	= '10240';
				$config['file_name'] = "1.jpg";

				while(file_exists($config['upload_path'].$config['file_name']))
				{						
					$name = $config['file_name'];
					$number = explode(".", $name);
					$number[0] = (int)$number[0];
					$number[0]++;
					$name = implode(".", $number);
					$config['file_name'] = $name;
				}

				$this->load->library('upload', $config);
				$titlu = $this->input->post('titlu');

				if ( !$this->upload->do_upload() || empty($titlu))
				{

					$error = array('error' => $this->upload->display_errors());
					$this->load->view('upload', $error);
				}
				else
				{

					$file_data = $this->upload->data();

					$sursa = $file_data['file_name'];
					$utilizator = $user;

					$this->load->model('set');

					$concurs = -1;

					if(is_null($id))
						$concurs = 0;
					else
						$concurs = $id;

					$this->load->model('get');
					if(count($this->get->number_of_posts($user)) > 4)
					{
						redirect(base_url('main/error'));
					}
					if ($this->set->insert_post($titlu, $utilizator, $sursa, $concurs)) {
						redirect(base_url());
					} else
						echo "EROARE";
				}

			} 
			else 
			{
				redirect(base_url());
			} 
	}



	public function like($id){

			$user = $this->session->userdata("logged_in");
			$tip = $this->session->userdata("tip");
			$this->load->model('get');
			$concurs = $this->get->concurs_id($id);
			$res = $this->get->verify_exist($id,'post');
		if($user && !empty($res[0]) && is_numeric($id) && ($tip == 2 || $concurs[0]['concurs']==0) ) {

		$post = $this->get->post($id); 
		$data = $post[0];


		$ok = 0;

		$like = $data['likeutilizatori'];
		$dislike = $data['dislikeutilizatori'];

			$a = strtok($dislike,"*");
			while(!empty($a))
			{
				
				if(strcmp($a,$user)==0)
					{$ok=2;
						break;}
				$a = strtok("*");
			}

			$a = strtok($like,"*");
			while(!empty($a))
			{
				
				if(strcmp($a,$user)==0)
					{$ok=1;
						break;}					
				$a = strtok("*");
			}

		if($ok==0)
		{$this->load->model("set");
		$this->set->upvote($data['id']);
		$this->set->score($data['id'],1);
		$this->set->addlike($user,$data['id']);
	
		redirect(base_url("display/post/".$id));

		}
		else
			if($ok==2)
				{
					$this->load->model("set");
					$this->set->upvote($data['id']);
					$this->set->addlike($user,$data['id']);
							$this->set->score($data['id'],2);
					$this->set->removedislike($user,$data['id']);
					$this->set->removedownvote($data['id']);
					
					redirect(base_url("display/post/".$id));

				}
		else
			redirect(base_url("display/post/".$id));

		}
		else
			$this->load->view('sumerror');

	}

	public function delete($id){

		$user = $this->session->userdata("logged_in");
		$tip = $this->session->userdata("tip");
		$this->load->model('get');
		$this->load->model('set');
		$post = $this->get->post($id);
		if($user == $post[0]['utilizator'] || $tip == 2)
		{
			$this->set->delete_post($id);
			$this->set->delete_comments($id);
			redirect(base_url());
		}
		else
			$this->load->view('sumerror');

	}

	public function delete_comment($id){

		$user = $this->session->userdata("logged_in");
		$tip = $this->session->userdata("tip");
		$this->load->model('get');
		$this->load->model('set');
		$comment = $this->get->comment($id);
		if($user == $comment[0]['utilizator'] || $tip == 2)
		{
			$this->set->delete_comment($id);
			redirect(base_url('display/post/'.$comment[0]['postid']));
		}
		else
			$this->load->view('sumerror');

	}

	public function dislike($id){

			$user = $this->session->userdata("logged_in");
			$tip = $this->session->userdata("tip");
			$this->load->model('get');
			$concurs = $this->get->concurs_id($id);
			$res = $this->get->verify_exist($id,'post');
		if($user && !empty($res[0]) && is_numeric($id) && ($tip == 2 || $concurs[0]['concurs']==0) ) {

		$this->load->model("get");
		$post = $this->get->post($id); 
		$data = $post[0];

		$ok = 0;

		$like = $data['likeutilizatori'];
		$dislike = $data['dislikeutilizatori'];

			$a = strtok($dislike,"*");
			while(!empty($a))
			{
				if(strcmp($a,$user)==0)
					{$ok=1;
						break;}
				$a = strtok("*");
			}

			$a = strtok($like,"*");
			while(!empty($a))
			{
				if(strcmp($a,$user)==0)
					{$ok=2;
						break;}					
				$a = strtok("*");
			}

		if($ok==0)
		{$this->load->model("set");
		$this->set->downvote($data['id']);
		$this->set->adddislike($user,$data['id']);
		$this->set->score($data['id'],-1);
	
		redirect(base_url("display/post/".$id));

		}

		else
			if($ok==2)
				{
					$this->load->model("set");
					$this->set->downvote($data['id']);
					$this->set->adddislike($user,$data['id']);
							$this->set->score($data['id'],-2);
					$this->set->removelike($user,$data['id']);
					$this->set->removeupvote($data['id']);
					
					redirect(base_url("display/post/".$id));

				}
		else
			redirect(base_url("display/post/".$id));

		}
		else
			$this->load->view('sumerror');

	}


	public function addcomment($pid)
	{	

		$user = $this->session->userdata("logged_in");
		$this->load->model('get');
		$res = $this->get->verify_exist($pid,'post');
		if($user && is_numeric($pid) && !empty($res[0]))
		{
		$this->form_validation->set_rules("text","Text","required");
		$this->form_validation->set_message("required", "Campul %s este obligatoriu.");
		
		if($this->form_validation->run() == FALSE){
			redirect(base_url("display/post/".$pid));
		} else {
			$text = $this->input->post('text',TRUE);
			
			$this->load->model("set");
			if ($this->set->insert_comment($text, $pid, $user)) {
				redirect(base_url("display/post/".$pid));
			} else
				echo "EROARE";
		}
	}
		else
			$this->load->view('sumerror');
	}

	public function settings($id = NULL){

		if($id == NULL){
		$user = $this->session->userdata("logged_in");
		if($user){
		$this->load->model('get');
		$data['avatar'] = $this->get->avatar($user);
		$this->load->view('settings',$data);}
		else
			redirect(base_url());}
		else
			$this->load->view('sumerror');
	}


	public function avatar($id = NULL){
		if($id == NULL){
		$user = $this->session->userdata("logged_in");
		if($user) {
			
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'jpg|png';
		$config['max_size']	= '10240';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$config['file_name'] = "1.jpg";
 
		while(file_exists($config['upload_path'].$config['file_name']))
			{
				$name = $config['file_name'];
				$number = explode(".", $name);
				$number[0] = (int)$number[0];
				$number[0]++;
				$name = implode(".", $number);
				$config['file_name'] = $name;
			}

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());

			$this->load->model('get');
		$data['avatar'] = $this->get->avatar($user);
		$this->load->view('settings',$data);
		}
		else
		{

			$file_data = $this->upload->data();

			$sursa = $file_data['file_name'];
			$utilizator = $user;
			$titlu = "Avatar";
			$this->load->model('set');
			$concurs = -1;
			$this->set->delete_prev_avatar($user);


			if ($this->set->insert_post($titlu, $utilizator, $sursa, $concurs)) {
				redirect(base_url('user/settings'));
			} else
				echo "EROARE";
		}

	} 
	else {
		redirect(base_url());
	} }
	else
		$this->load->view('sumerror');

	}




}
?>