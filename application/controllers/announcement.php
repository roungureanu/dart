<?php

	Class Announcement extends CI_Controller{

		public function index(){
			$this->load->view('sumerror');
		}

		public function createannouncement($id = NULL){
		$user = $this->session->userdata("logged_in");
		$tip = $this->session->userdata("tip");
		
		if($user && $tip == 2 && $id == NULL){
		$this->form_validation->set_rules("titluanunt","Titlu","required");
		$this->form_validation->set_rules("textanunt","Text","required");

		$this->form_validation->set_message("required", "Campul %s este obligatoriu.");
		
		if($this->form_validation->run() == FALSE){
			$this->load->view('createannouncement');
		} else {
			$titlu = $this->input->post('titluanunt');
			$text = $this->input->post('textanunt');
			$this->load->model("set");
			if ($this->set->insert_announcement($titlu, $text)) {
				redirect(base_url());
			} else
				echo "EROARE";

		}
		}
		else
			$this->load->view('sumerror');
	}

	public function noutati($id = NULL)
	{

			$this->load->model("get");
			$this->load->model("set");

		if($id == NULL || is_numeric($id)){
			$this->load->library('pagination');

	$config['base_url'] = base_url("/announcement/noutati");
	$config['total_rows'] = $this->db->get('anunturi')->num_rows();
	$config['per_page'] = 15; 
	$config['num_links'] = 2;
	$config['cur_tag_open'] = '<b style="color:white">';
	$config['cur_tag_close'] = '</b>';
	$config['display_pages'] = TRUE;

	$this->pagination->initialize($config); 

		$data = $this->db->get('anunturi', $config['per_page'],  $this->uri->segment(3));

	$x=array();
	$k=0;
	foreach($data->result_array() as $row)
		{$x['posts'][$k]['id'] = $row['id'];
		$x['posts'][$k]['data'] = $row['data'];
		$x['posts'][$k]['titlu'] = $row['titlu'];
		$k++;}

	$x['links'] = $this->pagination->create_links();

	$this->load->view('noutati',$x);
}
	else
		$this->load->view('sumerror');
	}

	public function anunt($id){

		$this->load->model('get');
		$res = $this->get->verify_exist($id,'anunturi');
		if(is_numeric($id) && !empty($res[0]) ){
		$data['noutati'] = $this->get->anunt($id);
		$this->load->view("anunt",$data);
		}
		else
			$this->load->view('sumerror');

	}

	}
?>