<?php

	Class Contests extends CI_Controller{

		public function index(){
			$this->load->view('sumerror');
		}

		public function create($id = NULL){
		$user = $this->session->userdata("logged_in");
		$tip = $this->session->userdata("tip");

		if($user && $tip==2 && $id==NULL){
		$this->form_validation->set_rules("nume","Nume","required");
		$this->form_validation->set_rules("tema","Tema","required");
		$this->form_validation->set_rules("detalii","Detalii","required");
		$this->form_validation->set_rules("datasf","Data de sfarsit","require|callback_valid");
		$this->form_validation->set_message("required", "Campul - %s - este obligatoriu.");
		
		if($this->form_validation->run() == FALSE){
			$this->load->view('createcontest');
		} else {
			$nume = $this->input->post('nume');
			$tema = $this->input->post('tema');
			$data = $this->input->post('datasf');
			$detalii = $this->input->post('detalii');

			$datas = explode("/", $data);
			$aux = $datas[0];
			$datas[0] = $datas[1];
			$datas[1] = $aux;

			//$aud = $data[0];
			//$datas[0] = $datas[2];
			//$datas[2] = $aux;

			$datasf = implode("/", $datas);

			$this->load->model("set");
			if ($this->set->insert_contest($nume, $tema, $detalii, date('Y-m-d'), date('y/m/d', strtotime(str_replace('/', '-', $datasf))))) 
			{
				redirect(base_url());
			} else
				echo "EROARE";

			}
		}
		else
			$this->load->view('sumerror');
		}

		public function valid($datasf){
			$data = explode("/", $datasf);
			if(isset($data[0],$data[1],$data[2])){
				if(checkdate($data[0], $data[1], $data[2]) == true)
					return true;
				else
				{	$this->form_validation->set_message("valid","Data de sfarsit introdusa este invalida.");
					return false;}
			}
			else{
				$this->form_validation->set_message("valid","Data de sfarsit introdusa este invalida.");
				return false;}
		}

		public function contestslist($id = NULL){
			if($id == NULL || is_numeric($id)){
			$this->load->model("get");
			$this->load->library('pagination');


	$config['base_url'] = base_url("/contests/contestslist");
	$config['total_rows'] = $this->db->get('concurs')->num_rows();
	$config['per_page'] = 15; 
	$config['num_links'] = 2;
	$config['cur_tag_open'] = '<b style="color:white">';
	$config['cur_tag_close'] = '</b>';
	$config['display_pages'] = TRUE;

	$this->pagination->initialize($config); 

	$this->db->order_by("datasfarsit", "desc"); 
	$data = $this->db->get('concurs', $config['per_page'],  $this->uri->segment(3));

	$k=0;
	$x=array();
	foreach($data->result_array() as $row){
		$x['posts'][$k]['id'] = $row['id'];
		$x['posts'][$k]['nume'] = $row['nume'];
		$x['posts'][$k]['data'] = $row['data'];
		$x['posts'][$k]['datasf'] = $row['datasfarsit'];
		$k++;
		}
	$x['links'] = $this->pagination->create_links();
	$this->load->view('contests',$x);
}
	else
		$this->load->view('sumerror');
		}

		public function contest($id){

			$this->load->model('get');
			$res = $this->get->verify_exist($id,'concurs');

			if(is_numeric($id) && !empty($res[0]) ){
			$data['post'] = $this->get->contest($id);
			$data['winner'] = $this->get->winner($id);
			$this->load->view('contest',$data);
			}
			else
				$this->load->view('sumerror');

		}



	}

?>