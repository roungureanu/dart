<?php $this->load->view('header'); ?>

<div class="wrapper">
	<div class="creation">
		
		<div class="loginTitle">
		<p id="loginTitle">Formular de logare</p>
		</div>
		
		<div class = "hrHorizontalGlobal"></div>

		<form action="<?php echo base_url("login/signin"); ?>" method="post">
			<div class="username">
				<p class="text">Utilizator:</p>
				<input type="text" name="utilizator" style="color:white" class="inputSt"></input>
				<?php echo form_error('utilizator',"<div style='color:orange; margin-top: 10px;'>","</div>"); ?>
			</div>

			<div class = "hrHorizontalGlobal"></div>
			
			<div class="parola">
				<p class="text">Parola:</p>
				<input type="password" name="parola" style="color:white" class="inputSt" style="margin-bottom:10px;"></input>
				<?php echo form_error('parola',"<div style='color:orange; margin-top: 10px;'>","</div>"); ?>
			</div>

			<div class = "hrHorizontalGlobal"></div>
			
			<div class="submit">
				<input type="submit" class="submitButton" value="Posteaza"></input>
			</div>
			
		</form>
	</div>
</div>




</body>