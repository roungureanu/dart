
<html>
<head>
<title>DArt</title>
<link rel="stylesheet" href="<?php echo base_url("/assets/css");?>/stylesheet.css" />

<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="<?php echo base_url("/assets/scripts");?>/hoverMenu.js"></script>

</head>
<body>

<div class="headbar">

<ul id = "nav">
<li class = "hoverli1">

<img src="<?php echo base_url("assets/Resources/Arrow.png"); ?>" id = "menuIcon"></img>

	<ul class = "menu1">
		<a href = "<?php echo base_url(); ?>" class = "navText"><li>Acasa</li></a>
		<a href = "<?php echo base_url("announcement/noutati"); ?>" class = "navText"><li>Noutati</li></a>
		<a href = "<?php echo base_url("contests/contestslist"); ?>" class = "navText"><li>Concursuri</li></a>
		<a href = "<?php echo base_url("display/globalimg"); ?>" class = "navText"><li>Postari</li></a>
		<a href = "<?php echo base_url("display/random"); ?>" class = "navText"><li>Aleatoriu</li></a>
		<a href = "<?php echo base_url("display/newimg"); ?>" class = "navText"><li>Cele mai noi</li></a>
	</ul>
</li>
	
<li>
	
</li>

<li class = "hoverli2">
<?php $user = $this->session->userdata('logged_in');
	$tip = $this->session->userdata('tip'); 
if($user) { ?>

<div style = "width:150px;"><img src="<?php echo base_url("assets/Resources/UserPanelIcon.png"); ?>" id="userPanelIcon"></img></div>

	<ul class = "menu2">
		<a href = "<?php echo base_url("user/upload"); ?>" class = "navText"><li>Upload</li></a>
		<a href = "<?php echo base_url("user/settings"); ?>" class = "navText"><li>User Panel</li></a>
	<?php 
	if($tip==2){?>	

		<a href = "<?php echo base_url("announcement/createannouncement"); ?>" class = "navText"><li>Adauga anunt</li></a>
		<a href = "<?php echo base_url("contests/create"); ?>" class = "navText"><li>Creeaza concurs</li></a>

	<?php } ?>
		<a href = "<?php echo base_url("login/logout"); ?>" class = "navText"><li>Sign Out</li></a>
	</ul>
 <?php }
else { ?>
<div style = "padding-right:20px;">
<a href="<?php echo base_url("login/signin"); ?>" class = "menuText" style="display:inline-block;">Sign in</a>
<p class = "menuText" style="display:inline-block;">/</p>
<a href="<?php echo base_url("register/inregistrare") ?>" class= "menuText" style="display:inline-block;">Register</a>
</div>
	<?php } ?>

</li>
</li>
</ul>
</div>
