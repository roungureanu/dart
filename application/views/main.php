<?php $this->load->view('header'); ?>
<div class="wrapper">

	<div class="header">
	<div id="logo"><img src="assets/Resources/LogoT2.png" style="max-width:100%; height:auto;"></img></div>
	</div>

	<div class="hrHorizontal"></div>
	
	<p class = "homepageDescription">Bun venit pe website-ul nostru. Aici veti putea participa la diferite concursuri, vota imagini si contribui site-ului incarcand imagini.</p>

	<div class="hrHorizontal"></div>
	
	<div class="content">
	<?php if(count($posts) > 0) {?>
		<div class="featuredMainContainer">
		<a href="<?php echo base_url("display/post/".$posts[0]['id']); ?>">
			<div id="featuredMain" style="background:url(uploads/<?php echo $posts[0]['sursa']; ?>); background-size:cover;">
			</div>
			</a>
		</div>
	<?php } ?>
	<div class="hrHorizontal"></div>
	
	<?php if(count($posts) > 1) {?>
	<div class="featuredSecContainer">
		<a href="<?php echo base_url("display/post/".$posts[1]['id']); ?>">
			<div id="featuredSecondary" style="background:url(uploads/<?php echo $posts[1]['sursa']; ?>); background-size:cover;">
			</div>
			</a>
		</div>
	<?php } ?>	
	<?php if(count($posts) > 2) {?>
	<div class="featuredSecContainer">
		<a href="<?php echo base_url("display/post/".$posts[2]['id']); ?>">
			<div id="featuredSecondary" style="background:url(uploads/<?php echo $posts[2]['sursa']; ?>); background-size:cover;">
			</div>
			</a>
		</div>
	<?php } ?>	
	<?php if(count($posts) > 3) {?>
	<div class="featuredSecContainer">
		<a href="<?php echo base_url("display/post/".$posts[3]['id']); ?>">
			<div id="featuredSecondary" style="background:url(uploads/<?php echo $posts[3]['sursa']; ?>); background-size:cover;">
			</div>
			</a>
		</div>
	<?php } ?>	
	
	
	
	<div class="hrHorizontal"></div>
	
	<div class="footer">
	
	<p class = "cpyRight"><a href = "<?php echo base_url("main/copyright"); ?>" id="copyright" >Copyrights&copy; 2013 - Prezent, DArt</a></p>

	</div>

</div>




</body>