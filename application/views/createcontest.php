<?php $this->load->view('header'); ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
<script>
  $(function() {
    $( "#datepicker" ).datepicker();
  });
  </script>

<div class="wrapper">

	<div class="creation">
		
		<form action="<?php echo base_url('contests/create'); ?>" method="post">
			<div class="title">
				<p class="text" style="color:orange">Nume:</p>
				<input type="text" name="nume" class="inputSt" style="color:white" placeholder="Insereaza numele aici..."></input>
				<?php echo form_error('nume',"<div style='color:orange'>","</div>");?>
			</div>

			<div class = "hrHorizontalGlobal"></div>

			<div class="title">
				<p class="text" style="color:orange">Tema:</p>
				<input type="text" name="tema" class="inputSt" style="color:white" placeholder="Insereaza tema aici..."></input>
				<?php echo form_error('tema',"<div style='color:orange'>","</div>");?>
			</div>
			
			<div class = "hrHorizontalGlobal"></div>
			
			<div class="descriereConcurs">
				<p class="text" style="color:orange">Detalii:</p>
				<textarea name="detalii"  class="textArea"></textarea>
				<?php echo form_error('detalii',"<div style='color:orange'>","</div>"); ?>
			</div>

			<div class = "hrHorizontalGlobal"></div>

			<div class="title">
				<p class="text" style="color:orange">Data de sfarsit:</p>
				<input type="text" name="datasf" class="inputData" id="datepicker">
				<?php echo form_error('datasf',"<div style='color:orange'>","</div>"); ?>
			</div>

			<div class = "hrHorizontalGlobal"></div>

			<div class="submitAnnouncement">
				<input type="submit" class="submitButton" value="Posteaza"></input>
			</div>
		</form>
		
	</div>

</div>



</body>