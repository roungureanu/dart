<?php $this->load->view('header'); ?>

<div class="wrapper">

	<div class="creation">
		<?php echo form_open_multipart('user/upload/'.$this->uri->segment(3));?>

			<div class="titleUpload">
				<p class="text">Titlu:</p>
				<input type="text" name="titlu" class="inputSt" placeholder="Insereaza titlul aici..." name="titlu"></input>
			</div>

			<div class = "hrHorizontalGlobal"></div>
			
			<div class="upload">
				<p class="text">Selectati imaginea:</p> <p class="text" style="font-size:12px; color:orange">Poti incarca maxim 5 imagini pe zi. Marimea maxima a unei imagini este de 10MB. Sunt permise doar imagini cu extensia JPG.</p>
				<input type="file" name="userfile" size="20" class="uploadInput"></input>
				
			</div>
			<?php //if(isset($error)) echo $error;?>
			<div class = "hrHorizontalGlobal"></div>

			<div class="submit">
			<input type="submit" class="submitButton" value="Posteaza"></input>
			</div>
		</form>
		
	</div>

</div>



</body>
