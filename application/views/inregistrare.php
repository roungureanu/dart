<?php $this->load->view('header'); ?>

<div class="wrapper">
    
    <div class="creation">
        
        <div class="title">
        <p id="loginTitle">Formular de inscriere</p>
        </div>

        <div class = "hrHorizontalGlobal"></div>
        
        <form action="<?php echo base_url("register/inregistrare"); ?>" method="post">

            <div class="username">
                <p class="text">Utilizator:</p>
                <input type="text" name="utilizator" style="color:white" class="inputSt"></input>
                <?php echo form_error('utilizator',"<div style='color:orange; margin-top: 10px;'>","</div>"); ?>
            </div>

            <div class = "hrHorizontalGlobal"></div>
            
            <div class="parola">
                <p class="text">Parola:</p>
                <input type="password" name="parola" style="color:white" class="inputSt" style="margin-bottom:10px;"></input>
                <?php echo form_error('parola',"<div style='color:orange; margin-top: 10px;'>","</div>"); ?>
                <p class="text">Confirma parola:</p>
                <input type="password" name="cparola" style="color:white" class="inputSt"></input>
                <?php echo form_error('cparola',"<div style='color:orange; margin-top: 10px;'>","</div>"); ?>
            </div>
            
            <div class = "hrHorizontalGlobal"></div>

            <div class="email">
                <p class="text">Email:</p>
                <input type="text" name="email" style="color:white" class="inputSt" style="margin-bottom:10px;"></input>
                <?php echo form_error('email',"<div style='color:orange; margin-top: 10px;'>","</div>"); ?>
                <p class="text">Confirma adresa de email:</p>
                <input type="text" name="cemail" style="color:white" class="inputSt"></input>
                <?php echo form_error('cemail',"<div style='color:orange; margin-top: 10px;'>","</div>"); ?>
            </div>

            <div class = "hrHorizontalGlobal"></div>
            
            <div class="name">
                <p class="text">Nume:</p>
                <input type="text" name="nume" style="color:white" class="inputSt" style="margin-bottom:10px;"></input>
                <?php echo form_error('nume',"<div style='color:orange; margin-top: 10px;'>","</div>"); ?>
                <p class="text">Prenume:</p>
                <input type="text" name="prenume" style="color:white" class="inputSt"></input>
                <?php echo form_error('prenume',"<div style='color:orange; margin-top: 10px;'>","</div>"); ?>
            </div>

            <div class = "hrHorizontalGlobal"></div>
            
            <div class="submit">
                <input type="submit" class="submitButton" value="Posteaza"></input>
            </div>
            
        </form>
        
    </div>
    
</div>



</body>