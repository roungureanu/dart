<?php $this->load->view('header'); ?>

<div class="wrapper">

	<div class="creation">
		
		<form action="<?php echo base_url('announcement/createannouncement'); ?>" method="post">
			<div class="title">
				<p class="text" style="color:orange">Titlu:</p>
				<input type="text" name="titluanunt" class="inputSt" style="color:white" placeholder="Insereaza titlul aici..."></input>
				<?php echo form_error('titluanunt',"<div style='color:orange'>","</div>");?>
			</div>
			
			<div class = "hrHorizontalGlobal"></div>

			<div class="descriereConcurs">
				<p class="text" style="color:orange">Descriere:</p>
				<textarea name="textanunt"  class="textArea"></textarea>
				<?php echo form_error('textanunt',"<div style='color:orange'>","</div>"); ?>
			</div>

			<div class = "hrHorizontalGlobal"></div>
			
			<div class="submitAnnouncement">
				<input type="submit" class="submitButton" value="Posteaza"></input>
			</div>
		</form>
		
	</div>

</div>



</body>