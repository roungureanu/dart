<?php $this->load->view('header'); ?>
<?php $x = strrev($post['data']);
$user = $this->session->userdata("logged_in");
$type = $tip = $this->session->userdata("tip");
$aux='a';
$aux = $x[0];
$x[0] = $x[1];
$x[1] = $aux;

$aux = $x[3];
$x[3] = $x[4];
$x[4] = $aux;

$aux = $x[6];
$x[6] = $x[9];
$x[9] = $aux;

$aux = $x[7];
$x[7] = $x[8];
$x[8] = $aux;
?>

<div class="wrapper">



<table style="width:100%; margin:5px;">
	<tr>

		<td id="details">
			<div id="inWrapper">
				<div class="titlePost">

				<?php echo $post['titlu']; ?>
				<br class="clear">
				
				</div>
				
				<div class="uplDate">
					<span class = "lAlign">Data postarii: </span> 
					<span class = "rAlign"><?php echo $x; ?></span>
				<br class="clear">
				
				</div>
				

				<div class="likes">
				
				<span class = "lAlign">Numar like-uri: </span> <span class = "likesSpan"> <?php echo $post['likes']; ?> </span>
				<br class="clear">
				
				</div>
				
				<div class="dislikes">
				
				<span class = "lAlign">Numar dislike-uri: </span> <span class = "dislikesSpan"> <?php echo $post['dislikes']; ?> </span>
				<br class="clear">
				
				</div>
				<?php if(!empty($post['concurs']) && $this->session->userdata("tip") == 2){?>
				<div class="vote">
					<span class = "likeSpan"><a href="<?php echo base_url("user/like/".$post['id']); ?>" class = "like">Like</a></span> 
					<?php if($user == $post['utilizator'] || $type == 2) {?>
					<span class = "deleteSpan"><a href="<?php echo base_url("user/delete/".$post['id']); ?>" class = "delete">*Delete*</a></span>
					<?php } ?>
					<span class= "dislikeSpan"><a href="<?php echo base_url("user/dislike/".$post['id']); ?>" class = "dislike">Dislike</a></span>
				<br class="clear">
				
				</div>
				<?php }else 
				if(empty($post['concurs'])) { ?>
				<div class="vote">
					<span class = "likeSpan"><a href="<?php echo base_url("user/like/".$post['id']); ?>" class = "like">Like</a></span> 
					<?php if($user == $post['utilizator'] || $type == 2) {?>
					<span class = "deleteSpan"><a href="<?php echo base_url("user/delete/".$post['id']); ?>" class = "delete">*Delete*</a></span>
					<?php } ?>
					<span class="dislikeSpan"><a href="<?php echo base_url("user/dislike/".$post['id']); ?>" class = "dislike">Dislike</a></span>
				<br class="clear">
				</div>
				<?php } ?>
				<div class="comments">
				
					<?php $k=-1; 
					if(count($comments) > 0) foreach ($comments as $comment) {$k++;?>
					<div class="comment">
						<span class="avatarZone">
							<div class = "avatar" style="background:url(<?php echo base_url("uploads/".$avatars[$k][0]['sursa']); ?>); background-size:cover;"></div>
						</span>
						<span class="commentZone">
							<div class="user"><?php echo $comment['utilizator']; ?></div>
							<div class="commentText"> <?php echo $comment['text']; ?> </div>
						</span>
						<?php if($user == $comment['utilizator'] || $type == 2){ ?>
						<span class="deleteZone">
							<a href="<?php echo base_url("user/delete_comment/".$comment['id']);?>">X</a>
						</span>
						<?php } ?>
					</div>
					<?php } ?>
				</div>	 
				
				<?php if($this->session->userdata("logged_in"))
				{?>
				<div class="postComment">
					<p class = "addComment">Adauga un commentariu:</p> 
					<p class = "commentWarning">Maxim 140 caractere!</p>
					
					 <form action="<?php echo base_url("user/addcomment/".$post['id']); ?>" method="post">
					<textarea name="text" class="textareaComment" maxlength="140"></textarea>
					<input type="submit" class="submitComment"></input>
					</form>
				</div>
				<?php }  ?>
				
			
		</td>
		

		
		<td>
			<div id="imageContainer">
			<img src="<?php echo base_url('/uploads/'.$post['sursa']); ?>" id="image"</img>
		</div>
		</td>

		
	</tr>
</table>

</div>


</body>