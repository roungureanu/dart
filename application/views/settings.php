<?php $this->load->view('header'); ?>
<div class="wrapper">


	<div class = "contentUP">
		<ul class = "headerUP">

		<li><div style="background-image:url(<?php if(count($avatar) > 0) echo base_url('/uploads/'.$avatar[0]['sursa']);?>); background-size:cover; background-position:center center;" class = "avatarUP"></div></li>
		<li><div class = "nameUP"><?php echo $this->session->userdata("logged_in");?></div></li>

		</ul>

		<div class = "hrHorizontalGlobal"></div>

		<div class="linkCont"><a href="<?php echo base_url('display/myimgs');?>" class = "link">Lucrari postate</a></div>

		<div class = "hrHorizontalGlobal"></div>

		<form action = "<?php echo base_url("change/password/".$this->session->userdata("logged_in")); ?>" method ="post">

			<div class = "field">
				<p class = "titleUP">Schimbare parola</p>

				<p class = "textUP">Introdu parola curenta:</p>
					<input type="password" class="inputSt" name = "parola">
					<?php echo form_error('parola',"<div style='color:orange'>","</div>"); ?>
				<p class = "textUP">Introdu noua parola:</p>
					<input type="password" class="inputSt" name = "nparola">
					<?php echo form_error('nparola',"<div style='color:orange'>","</div>"); ?>
				<p class = "textUP">Confirma noua parola:</p>
					<input type="password" class="inputSt" name = "ncparola">
					<?php echo form_error('ncparola',"<div style='color:orange'>","</div>"); ?>

				<div class = "submitUP"><input type="submit" class="submitButtonUP" value="Submit"></div>
			</div>

		</form>

		<div class = "hrHorizontalGlobal"></div>

		<form action = "<?php echo base_url("change/email/".$this->session->userdata("logged_in")); ?>" method ="post">

			<div class = "field">
				<p class = "titleUP">Schimbare email</p>

				<p class = "textUP">Introdu parola curenta:</p>
					<input type="password" class="inputSt" name = "parolae">
					<?php echo form_error('parolae',"<div style='color:orange'>","</div>"); ?>
				<p class = "textUP">Introdu noua adresa de email:</p>
					<input class="inputSt" name = "nemail">
					<?php echo form_error('nemail',"<div style='color:orange'>","</div>"); ?>
				<p class = "textUP">Confirma noua adresa de email:</p>
					<input class="inputSt" name = "ncemail">
					<?php echo form_error('ncemail',"<div style='color:orange'>","</div>"); ?>

				<div class = "submitUP"><input type="submit" class="submitButtonUP" value="Submit"></div>
			</div>

		</form>

		<div class = "hrHorizontalGlobal"></div>

		<?php echo form_open_multipart('user/avatar');?>

			<div class = "field">
				<p class = "titleUP">Schimbare avatar</p>

				<input type="file" name="userfile" size="20" class="uploadInput"></input>
				<p class = "textUP">Marime maxima - 100 x 100px</p>
				<?php //echo $error;?>
				<div class = "submitUP"><input type="submit" class="submitButtonUP" value="Submit"></div>
			</div>

		</form>

	</div>

</div>

</body>