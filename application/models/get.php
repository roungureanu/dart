<?php

class Get extends CI_Model {

	public function posts(){
		$result = $this->db->get("post");
		return $result->result_array();
	}

	public function contests(){
		$result = $this->db->get("concurs");
		return $result->result_array();
	}

	public function contest($id){
		$this->db->where("id", $id);
		$result = $this->db->get("concurs");
		return $result->result_array();
	}

	public function indexposts(){
		$this->db->order_by("score", "desc"); 
		$this->db->where('concurs', 0);
		$result = $this->db->get("post");
		return $result->result_array();
	}

	public function search(){
		$result = $this->db->get("search");
		return $result->result_array();
	}

	public function postid($sursa){
		$this->db->where("sursa",$sursa);
		$result = $this->db->get("post");
		return $result->result_array();
	}

	public function comment($id){
		$this->db->where("id",$id);
		$result = $this->db->get("comments");
		return $result->result_array();
	}

	public function comments($id){
		$this->db->where("postid",$id);
		$result = $this->db->get("comments");
		return $result->result_array();
	}


	public function noutati(){
		$result = $this->db->get("anunturi");
		return $result->result_array();
	}

	public function anunt($id){
		$this->db->where("id",$id);
		$result = $this->db->get("anunturi");
		return $result->result_array();
	}

	public function likes($utilizator){
		$this->db->where("utilizator",$utilizator);
		$result = $this->db->get("users");
		return $result->result_array();
	}

	public function concursuri(){
		$result = $this->db->get("concurs");
		return $result->result_array();
	}


	public function post($id){

		$this->db->where("id", $id);
		$result = $this->db->get("post");
		return $result->result_array();
	}

	public function number_of_posts($user)
	{
		date_default_timezone_set('Europe/Bucharest');
		$currentdate = date('Y-m-d');
		$this->db->where('utilizator', $user);
		$this->db->where('data', $currentdate);
		$result = $this->db->get("post");
		return $result->result_array();
	}	

	public function user($user){

		$this->db->where("utilizator",$user);
		$result = $this->db->get("users");
		return $result->result_array();

	}

	public function login($utilizator, $parola) {

		$this->db->where("utilizator",$utilizator);
		$result = $this->db->get("users");
		if($result->num_rows() == 1) {
			$users = $result->result_array();
			$user = $users[0];
			if ($user['parola'] == md5($parola)) {
				$u = array(
					'id' => $user['id'],
					 'utilizator' => $user['utilizator']
					 );
				return $u;
			} else { 
				return false;
			}
		} else {
			return false;
		}
	}

	public function winner($id){
		
		$this->db->select_max('likes');
		$this->db->where('concurs',$id);
		$res = $this->db->get('post');
		$x = $res->result_array();

		if(!empty($max))
			$max = 0;
		else
		$max = $x[0]['likes'];

		$this->db->select('utilizator');
		$this->db->where('likes',$max);
		$this->db->where('concurs',$id);
		$result = $this->db->get('post');
		return $result->result_array();
	}

	public function avatar($user){

		$this->db->where('utilizator',$user);
		$this->db->where('titlu', "Avatar");
		$result = $this->db->get('post');
		return $result->result_array();
	}

	public function verify_exist($id,$table){
		$this->db->where("id",$id);
		$result = $this->db->get($table);
		return $result->result_array();
	}

	public function concurs_id($id){
		$this->db->where("id",$id);
		$result = $this->db->get('post');
		return $result->result_array();
	}

	public function score($id){
		$this->db->where('id',$id);
		$result = $this->db->get('post');
		return $result->result_array();
	}
}

?>