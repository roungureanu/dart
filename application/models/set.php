<?php 

class Set extends CI_Model {

	public function insert_post($titlu, $utilizator, $sursa, $concurs)
	{

		date_default_timezone_set('Europe/Bucharest');
		$currentdate = date('Y-m-d');

		$dataP = array(
			"titlu" => $titlu,
			"utilizator" => $utilizator,
			"sursa" => $sursa,		
			"data" => $currentdate,
			"concurs" => $concurs
			);

		return $this->db->insert("post", $dataP);

	}

	public function insert_comment($text, $pid, $user){

		$data = array(
			"utilizator" => $user,
			"text" => $text,
			"postid" => $pid
			);

		return $this->db->insert("comments", $data);
	}	


	public function insert_announcement($titlu, $text){

		date_default_timezone_set('Europe/Bucharest');
		$currentdate = date("Y-m-d");

		$data = array(
			"titlu" => $titlu,
			"text" => $text,
			"data" => $currentdate
			);
		return $this->db->insert("anunturi", $data);
	}

	public function addlike($utilizator,$pid)
	{
		$this->db->where("id",$pid);
		$this->db->select("likeutilizatori");
		$result = $this->db->get("post");
		$plikes = $result->result_array();
		$likes = $plikes[0]['likeutilizatori'];
		$likes = $likes."*".$utilizator."*";
		$data = array(
			"likeutilizatori" => $likes
			);
		$this->db->where("id",$pid);
		return $this->db->update("post",$data);
	}

	public function removelike($utilizator,$pid)
	{
		$this->db->where("id",$pid);
		$this->db->select("likeutilizatori");
		$result = $this->db->get("post");
		$plikes = $result->result_array();
		$likes = $plikes[0]['likeutilizatori'];
		
		$user = "*".$utilizator."*";

		$likes = str_replace($user,"",$likes);

		$data = array(
			"likeutilizatori" => $likes
			);
		$this->db->where("id",$pid);
		return $this->db->update("post",$data);
	}

	public function removedislike($utilizator,$pid)
	{
		$this->db->where("id",$pid);
		$this->db->select("dislikeutilizatori");
		$result = $this->db->get("post");
		$plikes = $result->result_array();
		$likes = $plikes[0]['dislikeutilizatori'];
		
		$user = "*".$utilizator."*";

		$likes = str_replace($user,"",$likes);

		$data = array(
			"dislikeutilizatori" => $likes
			);

		$this->db->where("id",$pid);
		return $this->db->update("post",$data);
	}

	public function adddislike($utilizator,$pid)
	{
		$this->db->where("id",$pid);
		$this->db->select("dislikeutilizatori");
		$result = $this->db->get("post");
		$plikes = $result->result_array();
		$likes = $plikes[0]['dislikeutilizatori'];
		$likes = $likes."*".$utilizator."*";
		$data = array(
			"dislikeutilizatori" => $likes
			);
		$this->db->where("id",$pid);
		return $this->db->update("post",$data);
	}

	public function upvote($id){

		$this->db->where("id",$id);
		$this->db->select("likes");
		$result = $this->db->get("post");
		$q = $result->result_array();
		$x = $q[0]['likes'];

		$x=$x+1;
		
		$this->db->where("id",$id);
		$data = array(
				'likes' => $x
			);
		return $this->db->update("post", $data);
	}

	public function removeupvote($id){

		$this->db->where("id",$id);
		$this->db->select("likes");
		$result = $this->db->get("post");
		$q = $result->result_array();
		$x = $q[0]['likes'];
		if($x)
		$x=$x-1;
		
		$this->db->where("id",$id);
		$data = array(
				'likes' => $x
			);
		return $this->db->update("post", $data);
	}

	public function downvote($id){

		$this->db->where("id",$id);
		$this->db->select("dislikes");
		$result = $this->db->get("post");
		$q = $result->result_array();
		$x = $q[0]['dislikes'];

		$x=$x+1;
		
		$this->db->where("id",$id);
		$data = array(
				'dislikes' => $x
			);
		return $this->db->update("post", $data);
	}

	public function removedownvote($id){

		$this->db->where("id",$id);
		$this->db->select("dislikes");
		$result = $this->db->get("post");
		$q = $result->result_array();
		$x = $q[0]['dislikes'];
		if($x)
		$x=$x-1;
		
		$this->db->where("id",$id);
		$data = array(
				'dislikes' => $x
			);
		return $this->db->update("post", $data);
	}

	public function insert_user($utilizator, $parola, $email, $nume, $prenume, $tip){

		$data = array(
			"utilizator" => $utilizator,
			"parola" => md5($parola),
			"email" => $email,
			"nume" => $nume,
			"prenume" =>$prenume,
			"tip" => $tip
			);

		return $this->db->insert("users", $data);

	}

	public function first_avatar($utilizator){

		$data = array(
			"utilizator" => $utilizator,
			"titlu" => "Avatar",
			"sursa" => "Avatar.jpg",
			"concurs" => -1
			);

		return $this->db->insert('post',$data);
	}

	public function delete_post($id) {

		$this->db->where("id", $id);
		return $this->db->delete("post");
	}

	public function delete_comment($id) {

		$this->db->where("id", $id);
		return $this->db->delete("comments");
	}

	public function delete_comments($id) {

		$this->db->where("postid", $id);
		return $this->db->delete("comments");
	}


	public function update_post($titlu, $text, $id, $utilizator) {

		$this->db->where("id",$id);
		$data = array(
			"titlu" => $titlu,
			"text" => $text,
			"utilizator" => $utilizator
			);
		return $this->db->update("posts", $data);
	}

	public function insert_contest($nume, $tema, $detalii, $datacurenta, $datasf) {
		$data = array(
			"nume" => $nume,
			"tema" => $tema,
			"detalii" => $detalii,
			"data" => $datacurenta,
			"datasfarsit" =>$datasf
			);
		return $this->db->insert("concurs",$data);
	}

	public function delete_prev_avatar($nume){
		$this->db->where('utilizator',$nume);
		$this->db->where('titlu',"Avatar");
		$this->db->delete('post');
	}

	public function update_password($parola){
		$utilizator = $this->session->userdata("logged_in");
		$data = array(
			'parola' => md5($parola)
			);
		$this->db->where('utilizator',$utilizator);
		return $this->db->update('users',$data);
	}

	public function update_email($email){
		$utilizator = $this->session->userdata("logged_in");
		$data = array(
			'email' => $email
			);
		$this->db->where('utilizator',$utilizator);
		return $this->db->update('users',$data);
	}

	public function score($id,$number){
		$this->load->model('get');
		$score = $this->get->score($id);
		$scr = $score[0]['score'];
		$scr = $scr + $number;
		$data = array(
			'score' => $scr
			);
		$this->db->where('id',$id);
		return $this->db->update('post',$data);
	}

}


?>